## Bug Report
<!--
  Thank you for taking the time to report a bug! Don't forget to fill in an
  appropriate title above and the information in the table below.
-->
### Info
<!--
  If you click the "Report A Bug" link in the Tildes ReExtended options page,
  the table below will automatically be filled with your details. That might be
  easier than filling it out manually.
-->

| Type | Value |
|------|-------|
| Operating System |  |
| Browser |  |
| Device |  |

### The Problem
<!--
  Please explain in sufficient detail what the problem is. When suitable,
  including an image or video showing the problem will also help immensely.
-->


### A Solution
<!--
  If you know of any possible solutions, feel free to include them. If the
  solution is just something like "it should work" then you can safely omit
  this section.
-->

